import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {EScreens} from './screens';
import {AuthScreen} from '../screens/AuthScreen';
import {NoInternetScreen} from '../screens/NoInternetScreen';
import {StartScreen} from '../screens/StartScreen';
import {useNavigation} from '@react-navigation/native';
import {useNetInfo} from '@react-native-community/netinfo';

const Stack = createStackNavigator();

export const Navigator = () => {
  const navigation = useNavigation<any>();
  const netInfo = useNetInfo();

  useEffect(() => {
    if (!netInfo.isConnected) {
      navigation.navigate(EScreens.NO_INTERNET);
    } else {
      navigation.navigate(EScreens.START);
    }
  }, [netInfo.isConnected]);

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name={EScreens.START} component={StartScreen} />
      <Stack.Screen name={EScreens.AUTH} component={AuthScreen} />
      <Stack.Screen name={EScreens.NO_INTERNET} component={NoInternetScreen} />
    </Stack.Navigator>
  );
};
