import React, {useEffect} from 'react';
import { Alert, StyleSheet, Text, TextInput, View } from "react-native";
import {useForm} from 'react-hook-form';
import {CustomButton} from '../components/CustomButton';
import {useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const AuthScreen = () => {
  const {register, handleSubmit, watch, setValue} = useForm();

  const navigation = useNavigation();

  const onSubmit = async (data: any) => {
    try {
      await AsyncStorage.setItem('login', data.name);
      navigation.goBack();
    } catch (e) {
      Alert.alert('Error');
    }
  };

  const onChangeText = (keyName: string, text: string) => {
    setValue(keyName, text);
  };

  useEffect(() => {
    register('name');
    register('password');
  }, [register]);

  return (
    <View style={styles.container}>
      <Text>login</Text>
      <TextInput
        style={styles.input}
        onChangeText={text => onChangeText('name', text)}
        value={watch('name')}
      />

      <Text>password</Text>
      <TextInput
        secureTextEntry={true}
        style={styles.input}
        onChangeText={text => onChangeText('password', text)}
        value={watch('password')}
      />

      <CustomButton title={'Login'} onPress={handleSubmit(onSubmit)} top={32} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 16,
    marginTop: 32,
  },
  input: {
    height: 40,
    marginVertical: 12,
    borderWidth: 1,
    padding: 10,
  },
});
